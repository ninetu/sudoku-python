sectors = [
	['A1','A2','A3','B1','B2','B3','C1','C2','C3'], ['A4','A5','A6','B4','B5','B6','C4','C5','C6'], ['A7','A8','A9','B7','B8','B9','C7','C8','C9'],
	['D1','D2','D3','E1','E2','E3','F1','F2','F3'], ['D4','D5','D6','E4','E5','E6','F4','F5','F6'], ['D7','D8','D9','E7','E8','E9','F7','F8','F9'],
	['G1','G2','G3','H1','H2','H3','I1','I2','I3'], ['G4','G5','G6','H4','H5','H6','I4','I5','I6'], ['G7','G8','G9','H7','H8','H9','I7','I8','I9']
]

def grid_values(grid):
	"""Convert grid string into {<box>: <value>} dict with '123456789' value for empties.

	Args:
		grid: Sudoku grid in string form, 81 characters long
	Returns:
		Sudoku grid in dictionary form:
		- keys: Box labels, e.g. 'A1'
		- values: Value in corresponding box, e.g. '8', or '123456789' if it is empty.
	"""
	
	''' Your solution here 
	.........
	.........
	.........
	.........
	.........
	.........
	'''
	rows = 'ABCDEFGHI'
	box = {}
	i = 0
	j = 0
	for c in list(grid):
		if j>0 and j%9 == 0:
			i = i+1
		a = rows[i]
		# box[a+str((j%9)+1)] = c
		if c == '.':
			box[a+str((j%9)+1)] = '123456789'
		else:
			box[a+str((j%9)+1)] = c
		j = j+1
	return box

def eliminate(values):
	"""Eliminate values from peers of each box with a single value.

	Go through all the boxes, and whenever there is a box with a single value,
	eliminate this value from the set of values of all its peers.

	Args:
		values: Sudoku in dictionary form.
	Returns:
		Resulting Sudoku in dictionary form after eliminating values.
	"""
	rows = 'ABCDEFGHI'
	cols = '123456789'

	isFirst = False
	isFound = False
	for c in values:
		if len(values[c]) == 1:
			(row,col) = list(c)
			# print ('Found ' + values[c] + ' at (' + row + ',' + col + ')')

			#delete all Ax
			for i in cols:
				if i!=col:
					rc = row+i
					# print ('Scan ' + rc + ' then remove ' + values[c])
					if values[c] in list(values[rc]):
						values[rc] = values[rc].replace(values[c],"")

			#delete all xN
			for i in rows:
				if i!=row:
					rc = i+col
					# print ('Scan ' + rc + ' then remove ' + values[c])
					#values[rc] = values[rc].replace(values[c],"")
					if values[c] in list(values[rc]):
						values[rc] = values[rc].replace(values[c],"")

			#delete in minibox
			for sector in sectors:
				if c in sector:
					for sec in sector:
						if sec != c:
							if values[c] in list(values[sec]):
								values[sec] = values[sec].replace(values[c],"")
					break

	return values

def only_choice(values):
	"""Finalize all values that are the only choice for a unit.

	Go through all the units, and whenever there is a unit with a value
	that only fits in one box, assign the value to this box.

	Input: Sudoku in dictionary form.
	Output: Resulting Sudoku in dictionary form after filling in only choices.
	"""
	
	''' Your solution here 
	.........
	.........
	.........
	.........
	.........
	.........
	'''
	rows = 'ABCDEFGHI'
	for c in values:
		if len(values[c])>1:
			for sector in sectors:
				if c in sector:
					(row, col) = list(c)
					sectorRow = [row + str(i) for i in [1,2,3,4,5,6,7,8,9]]
					sectorCol = [i + col for i in list(rows)]
					sectorAll = list(set().union(sector,sectorRow,sectorCol))
					sectorAll = sector
					#print ('at: ' + c + ' then:')
					#print (sectorAll)

					for i in values[c]:
						isOnly = True
						#print ('look for: ' + i + ' in ' + str(sectorAll))
						for sec in sectorAll:
							if c != sec:
								for j in list(values[sec]):
									#print (i + ' vs ' + j)
									if i == j:
										isOnly = False
										break
								else:
									continue
								break
						
						#print (i + ' : ' + str(isOnly))
						if isOnly:
							values[c] = i
	return values

def isSolved(values):
	res = True
	for c in values:
		if len(values[c]) != 1:
			res = False
			break
	return res

def isInvalid(values):
	res = False
	for c in values:
		if len(values[c]) < 1:
			res = True
			break
	return res


def reduce_puzzle(values):
	"""
	Iterate eliminate() and only_choice(). If at some point, there is a box with no available values, return False.
	If the sudoku is solved, return the sudoku.
	If after an iteration of both functions, the sudoku remains the same, return the sudoku.
	Input: A sudoku in dictionary form.
	Output: The resulting sudoku in dictionary form.
	"""
	
	''' Your solution here 
	.........
	.........
	.........
	.........
	.........
	.........
	'''
	isFirst = True
	isLoop = False
	while isFirst or isLoop:
		isFirst = False
		strBefore = str(values)
		values = eliminate(values)
		values = only_choice(values)
		if isInvalid(values) == True:
			return False
		elif strBefore != str(values):
			isLoop = True
		elif isSolved(values) == True:
			return values
		elif strBefore == str(values):
			return values
		else:
			return False
	
#2. function.py ----------------------------
# 2.1 implement search() using Depth First Search Algorithm
#from utils import *
def search(values):
	"Using depth-first search and propagation, create a search tree and solve the sudoku."
	# First, reduce the puzzle using the previous function
	# Search and Choose one of the unfilled squares with the fewest possibilities
	# Now use recursion to solve each one of the resulting sudokus, 
	# and if one returns a value (not False), return that answer!
	
	''' Your solution here 
	.........
	.........
	.........
	.........
	.........
	.........
	'''
	res1 = reduce_puzzle(values)
	if res1 == False:
		return False
	else:
		if isSolved(values):
			return values
		else:
			arSize = {}
			for c in values:
				if len(values[c]) > 1:
					arSize[c] = len(values[c])
			arSize = sorted(arSize, key=arSize.get)
			for c in arSize:
				for i in list(values[c]):
					newValues = values.copy()
					newValues[c] = i
					res = search(newValues)
					if res != False:
						return res
				return False